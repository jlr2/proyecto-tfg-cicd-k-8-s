package com.vilches.proyectovilches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.vilches.proyectovilches")
public class ProyectoVilchesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoVilchesApplication.class, args);
	}

}
