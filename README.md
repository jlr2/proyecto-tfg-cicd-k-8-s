# Vilches’ TechLab

![Texto alternativo](img/portada.PNG)

- Grado: Desarrollo de Aplicaciones Multiplataforma
- Profesor: José Luis Rodríguez Rodríguez
- Alumno: Ángel David Vilches Amaya

- Centro: IES Ramón del Valle-Inclán
- Dirección: Calle del Doctor Palomares García, 2, 41020, Sevilla
- Página web: [https://blogsaverroes.juntadeandalucia.es/iesvalleinclan/](https://blogsaverroes.juntadeandalucia.es/iesvalleinclan/)

## Índice

1. Introducción
2. Tecnologías
3. Desarrollo de la app
    - 3.1 Entorno de desarrollo
    - 3.2 Requisitos no funcionales
    - 3.3 Dominio de la aplicación
    - 3.4 Análisis del proyecto
    - 3.5 Diseño del proyecto
     - Diagrama de clases
     - La clase CorsFilter se utiliza para evitar errores relacionados con CORS.
     - Capas de la aplicación
   - Interfaz gráfica
4. Infraestructura de una app
   - 4.1 Kubernetes
     - 4.1.1 Componentes
       - Pod
       - ReplicaSet
       - Service
       - Deployment
       - PersistentVolumeClaim
       - Ingress Controller
     - 4.1.2 YAML
   - 4.2 Gitlab
     - 4.2.1 Pipeline
5. Manual de usuario
6. Problemas surgidos durante su desarrollo
7. Mejoras posibles a realizar en el proyecto
8. Conclusiones
9. Difusión del proyecto
10. Bibliografía/Webgrafía
